<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Admin Crud Operations</title>
<style>
body {
     background-color: lightblue;
  }
h1 {
    text-align: center;
}
 
     
  
</style>
</head>
<body>
<h1>Admin Crud Operations Page</h1>
Hi,<%=(String)session.getAttribute("username") %>
<ul class="mylinks">
<li><a href="admin" onMouseOver="this.style.color='red'"  
                    onMouseOut="this.style.color='green'"  >Home</a></li>
<li><a href="inserthoteldetails" onMouseOver="this.style.color='red'" onMouseOut="this.style.color='green'" >Add HotelDetails</a></li>                 
<li><a href="logout.jsp" onMouseOver="this.style.color='red'" onMouseOut="this.style.color='green'" >Logout</a></li>
</ul>               
<hr/>${msg }
<table border="20" align="center">
<tr><th colspan="6">HotelDetails</th></tr>
<tr>
<th>HotelID</th>
<th>HotelName</th>
<th>City</th>
<th>RoomType</th>
<th>TotalRooms</th>
<th>RoomCost</th></tr>
<c:forEach items="${HotelDetails}" var="h">
<tr>
<td>${h.getHotelId()}</td>
<td>${h.getHotelName()}</td>
<td>${h.getCity()}</td>
<td>${h.getRoomType()}</td>
<td>${h.getTotalRooms()}</td>
<td>${h.getRoomCost()}</td>
</tr>
<td><a  href="updatetotalrooms?hotelid=${h.getHotelId()}&totalrooms=${h.getTotalRooms()}" onMouseOver="this.style.color='red'"  >Update TotalRooms</a></td>
<td><a  href="update?hotelid=${h.getHotelId()}&totalrooms=${h.getTotalRooms()}&roomcost=${h.getRoomCost()}" onMouseOver="this.style.color='red'"  >Update TotalRooms and roomcost</a></td>
<td><a  href="delete?hotelid=${h.getHotelId()}" onMouseOver="this.style.color='red'" >Delete</a></td>
</c:forEach>


</table>
</body>
</html>