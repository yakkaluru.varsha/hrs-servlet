<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Add HotelDetails</title>
<style>
body {
  background-color: lightblue;
}
h1 {
   text-align: center;
}
</style>
</head>
<body>
<h1>Add Hotel Details Page</h1>Hi,<%=(String)session.getAttribute("username") %><hr/>
<form action="add" method="post">
<table align="center">
<tr>
<td>Hotel ID:</td>
<td><input type="text" name="hotelid" autocomplete="off"/></td>
</tr>
<tr>
<td>Hotel Name:</td>
<td><input type="text" name="hotelname" autocomplete="off"/></td>
</tr>
<tr>
<td>City:</td>
<td><input type="text" name="city" autocomplete="off"/></td>
</tr>
<tr>
<td>Room Type:</td><td>
<input type="text" name="roomtype" autocomplete="off"/></td>
</tr>
<tr>
<td>Total Rooms:</td>
<td><input type="text" name="totalrooms" autocomplete="off"/></td>
</tr>
<tr>
<td>Room Cost:</td>
<td><input type="text" name="roomcost" autocomplete="off"/></td>
</tr>
<tr>
<td><input type="submit" value="add"/></td>
</tr>
</table>
</form>
</body>
</html>