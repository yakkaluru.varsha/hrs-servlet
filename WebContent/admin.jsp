<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Admin</title>
<style>
body {
  background-color: lightblue;
}
h1 {
   text-align: center;
}
 a { 
     text-decoration: none;
     background-color: red;
     color: white;
     text-transform: uppercase;
}

a:hover {
  background-color: #555;
}
     
 }
</style>
</head>
<body>

<h1>Admin Home page</h1>Hi,<%=(String)session.getAttribute("username") %>

<li><a href="admincrud"  onMouseOver="this.style.color='red'" onMouseOut="this.style.color='black'">Admin Crud </a></li>
<li><a href="logout.jsp" onMouseOver="this.style.color='red'" onMouseOut="this.style.color='black'" >Logout</a></li>

<hr/>
<table border="20" align="center">
<tr><th colspan="6"> Hotel Details</th></tr>
<tr>
<th>HotelID</th>
<th>HotelName</th>
<th>City</th>
<th>RoomType</th>
<th>TotalRooms</th>
<th>RoomCost</th></tr>
<c:forEach items="${HotelDetails}" var="hd">
<tr  style="text-align:center;">
<td>${hd.getHotelId()}</td>
<td>${hd.getHotelName()}</td>
<td>${hd. getCity()}</td>
<td>${hd.getRoomType()}</td>
<td>${hd.getTotalRooms()}</td>
<td>${hd.getRoomCost()}</td></tr>

</c:forEach>


</table>

</body>
</html>