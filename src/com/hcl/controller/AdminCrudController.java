package com.hcl.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hcl.dao.IHotelDao;
import com.hcl.dao.IHotelDaoImp;
import com.hcl.model.HotelDetails;


@WebServlet(name="AdminCrudController",urlPatterns= {"/admincrud","/home","/delete","/inserthoteldetails",
		"/updatetotalrooms","/update"})
public class AdminCrudController extends HttpServlet
{
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) 
			throws ServletException, IOException {

		String url = req.getServletPath();
		IHotelDao dao = new IHotelDaoImp();
		HotelDetails hd=new HotelDetails();
		if(url.equals("/admincrud"))
		{
			List<HotelDetails> list=dao.viewAll();
			req.setAttribute("HotelDetails", list);
			req.getRequestDispatcher("admincrud.jsp").include(req, resp);
		}
		else if(url.equals("/home"))
		{
			List<HotelDetails> list=dao.viewAll();
			req.setAttribute("hd", list);
			req.getRequestDispatcher("admin.jsp").forward(req, resp);
		}
		else if(url.equals("/delete"))
		{
			int hotelId=Integer.parseInt(req.getParameter("hotelid"));
			hd.setHotelId(hotelId);
			dao.removeHotelDetails(hd);
			PrintWriter out=resp.getWriter();
			resp.setContentType("text/html");
			out.print(hotelId+"deleted succesfully<br/>");
			List<HotelDetails> list=dao.viewAll();
			req.setAttribute("HotelDetails", list);
			req.getRequestDispatcher("admin.jsp").include(req, resp);
			
		}
		else if(url.equals("/inserthoteldetails"))
		{
			req.getRequestDispatcher("inserthoteldetails.jsp").forward(req, resp);
		}
		else if(url.equals("/signout"))
		{
			req.getRequestDispatcher("index.jsp").forward(req, resp);
		}
		else if(url.equals("/updatetotalrooms"))
		{
			
			int hotelId=Integer.parseInt(req.getParameter("hotelid"));
			int totalRooms=Integer.parseInt(req.getParameter("totalrooms"));
			HotelDetails hoteldetails=new HotelDetails(hotelId,totalRooms);
			req.setAttribute("hd", hoteldetails);
			req.getRequestDispatcher("updatetotalrooms.jsp").forward(req, resp);
		}
		else if(url.equals("/update"))
		{

			int hotelId=Integer.parseInt(req.getParameter("hotelid"));
			int totalRooms=Integer.parseInt(req.getParameter("totalrooms"));
			double roomcost=Double.parseDouble(req.getParameter("roomcost"));
		    HotelDetails hoteldetails=new HotelDetails(hotelId,totalRooms,roomcost);
			req.setAttribute("hd",hoteldetails);
			req.getRequestDispatcher("update.jsp").forward(req, resp);
		}
		
	
	}
	

}
