package com.hcl.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.hcl.dao.IHotelDao;
import com.hcl.dao.IHotelDaoImp;
import com.hcl.model.Admin;
import com.hcl.model.HotelDetails;


@WebServlet(name="AdminController", urlPatterns={"/login","/add","/updatetotalroomscon",
		"/updatetotalroomsandcostcon"})

public class AdminController extends HttpServlet
{
	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse res) 
			throws ServletException, IOException {
		

		String url = req.getServletPath();
		IHotelDao dao = new IHotelDaoImp();
		if(url.equals("/login"))
		{
			String userName=req.getParameter("username");
			String password=req.getParameter("password");
			HttpSession session=req.getSession(true);
			session.setAttribute("username", userName);
		    Admin admin=new Admin(userName, password);
		    int result=dao.adminLogin(admin);
		    if(result>0)
		    {
		    	List<HotelDetails> list=dao.viewAll();
		    	req.setAttribute("HotelDetails", list);
		    	req.getRequestDispatcher("admin.jsp").forward(req, res);
		    }
		    else
		    {
		    	req.setAttribute("error", "HI "+userName+ " ,Please Check Your Credentails");
		    	req.getRequestDispatcher("index.jsp").forward(req, res);
		    }
		}
		else if(url.equals("/add"))
		{
			int hotelId=Integer.parseInt(req.getParameter("hotelid"));
			String hotelName=req.getParameter("hotelname");
			String city=req.getParameter("city");
			String roomType=req.getParameter("roomtype");
			int totalRooms=Integer.parseInt(req.getParameter("totalrooms"));
			double cost=Double.parseDouble(req.getParameter("roomcost"));
			HotelDetails hoteldetails=new HotelDetails(hotelId, hotelName, city, roomType, totalRooms, cost);
			int result=dao.addHotelDetails(hoteldetails);
			if(result>0)
			{
				req.setAttribute("msg",+hotelId+ " Inserted Succesfully!!");
				List<HotelDetails> list=dao.viewAll();
				req.setAttribute("HotelDetails", list);
				req.getRequestDispatcher("admin.jsp").include(req, res);
				
			}
			else
			{
				req.setAttribute("msg",+hotelId+ " Duplicate Entry");
				req.getRequestDispatcher("admin.jsp").include(req, res);
			}
			
		}
		else if(url.equals("/updatetotalroomscon"))
		{
			int hotelId=Integer.parseInt(req.getParameter("hotelid"));
			int totalRooms=Integer.parseInt(req.getParameter("totalrooms"));
			HotelDetails hoteldetails=new HotelDetails(hotelId, totalRooms);
			dao.editTotalRooms(hoteldetails);
			List<HotelDetails> list=dao.viewAll();
			req.setAttribute("HotelDetails", list);
			req.getRequestDispatcher("admin.jsp").include(req, res);
		}
		else if(url.equals("/updatetotalroomsandcostcon"))
		{
			int hotelid=Integer.parseInt(req.getParameter("hotelid"));
			int totalRooms=Integer.parseInt(req.getParameter("totalrooms"));
			double roomCost=Double.parseDouble(req.getParameter("roomcost"));
			HotelDetails hoteldetails=new HotelDetails(hotelid,totalRooms,roomCost);
			dao.editTotalRooms(hoteldetails);
			List<HotelDetails> list=dao.viewAll();
			req.setAttribute("HotelDetails", list);
			req.getRequestDispatcher("admincrud.jsp").include(req, res);
		}
		
	}

}
