package com.hcl.model;

public class HotelDetails {


	   private int hotelId;
	   private String hotelName;
	   private String city;
	   private String roomType;
	   private int totalRooms;
	   private double roomCost;
	public HotelDetails(int hotelId, String hotelName, String city, String roomType, int totalRooms, double roomCost) {
		super();
		this.hotelId = hotelId;
		this.hotelName = hotelName;
		this.city = city;
		this.roomType = roomType;
		this.totalRooms = totalRooms;
		this.roomCost = roomCost;
	}
	public HotelDetails() {
		// TODO Auto-generated constructor stub
	}
	public HotelDetails(int hotelId, int totalRooms) {
		this.hotelId = hotelId;
		this.totalRooms = totalRooms;
	}
	
	public HotelDetails(int hotelId, int totalRooms, double roomCost) {
		
		this.hotelId = hotelId;
		this.totalRooms = totalRooms;
		this.roomCost = roomCost;
	}
	public int getHotelId() {
		return hotelId;
	}
	public void setHotelId(int hotelId) {
		this.hotelId = hotelId;
	}
	public String getHotelName() {
		return hotelName;
	}
	public void setHotelName(String hotelName) {
		this.hotelName = hotelName;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getRoomType() {
		return roomType;
	}
	public void setRoomType(String roomType) {
		this.roomType = roomType;
	}
	public int getTotalRooms() {
		return totalRooms;
	}
	public void setTotalRooms(int totalRooms) {
		this.totalRooms = totalRooms;
	}
	public double getRoomCost() {
		return roomCost;
	}
	public void setRoomCost(double roomCost) {
		this.roomCost = roomCost;
	}
	   
	   
}
