package com.hcl.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class DbUtil {
	private DbUtil()
	{
		
	}
	public static Connection getcon() throws SQLException, ClassNotFoundException
	{

		ResourceBundle rb=ResourceBundle.getBundle("db");
		Class.forName(rb.getString("driver"));
		Connection con=DriverManager.getConnection(rb.getString("url"),
				rb.getString("user"),rb.getString("pass"));
				
		return con;
	}
}
