package com.hcl.dao;

import java.util.List;

import com.hcl.model.Admin;
import com.hcl.model.HotelDetails;


public interface IHotelDao
{
	public int adminLogin(Admin admin);
	public List<HotelDetails> viewAll();
	public int removeHotelDetails(HotelDetails hotel);
	public int addHotelDetails(HotelDetails info);
	public int editTotalRooms(HotelDetails info);
	public int editTotalRoomsAndCost(HotelDetails info);
	
}
