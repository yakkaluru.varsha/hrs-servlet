package com.hcl.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.hcl.model.Admin;
import com.hcl.model.HotelDetails;
import com.hcl.util.DbUtil;
import com.hcl.util.QueryUtil;


public class IHotelDaoImp implements IHotelDao{


	int result;
	PreparedStatement pst;
	ResultSet rs;
	@Override
	public int adminLogin(Admin admin) {
		result=0;
		try {
			pst=DbUtil.getcon().prepareStatement(QueryUtil.adminLogin);
			pst.setString(1, admin.getUsername());
			pst.setString(2,admin.getPassword());
			rs=pst.executeQuery();
			while(rs.next())
			{
				result++;
			}
		} catch (ClassNotFoundException | SQLException e) {
			
			e.printStackTrace();
		}
		return result;
	}
	@Override
	public List<HotelDetails> viewAll() {
		
		List<HotelDetails> list=new ArrayList<HotelDetails>();
		try {
			pst=DbUtil.getcon().prepareStatement(QueryUtil.adminViewDetails);
			rs=pst.executeQuery();
			while(rs.next())
			{
				HotelDetails hotelDetails=new HotelDetails( rs.getInt(1),rs.getString(2), rs.getString(3),rs.getString(4),rs.getInt(5),rs.getDouble(6));
				list.add(hotelDetails);
			}
		} catch (ClassNotFoundException | SQLException e) {
			
			e.printStackTrace();
		}
		return list;
	}
	
	@Override
	public int removeHotelDetails(HotelDetails hotel) {
	
		result=0;
		try {
				pst=DbUtil.getcon().prepareStatement(QueryUtil.removeHotel);
				pst.setInt(1, hotel.getHotelId());
				pst.executeUpdate();
			} catch (ClassNotFoundException|SQLException e) {
				
				e.printStackTrace();
			}
		return result;
			
	}
	@Override
	public int addHotelDetails(HotelDetails info) {
		
		result =0;
		try {
			
			pst=DbUtil.getcon().prepareStatement(QueryUtil.addHotelDetails);
			pst.setInt(1, info.getHotelId());
			pst.setString(2, info.getHotelName());
			pst.setString(3, info.getCity());
			pst.setString(4,info.getRoomType());
			pst.setInt(5,info.getTotalRooms());
			pst.setDouble(6, info.getRoomCost());
			result=pst.executeUpdate();
			
		}
		catch(ClassNotFoundException | SQLException e) 
		{
			System.out.println("Exception occurs in add hotels");
			
		}
		finally
		{
			try {
				DbUtil.getcon().close();
				pst.close();
			}
			catch(ClassNotFoundException | SQLException e)
			{
				
			}
		}
		return result;
	}
	@Override
	public int editTotalRooms(HotelDetails info) {
	
		result=0;
		try {
			
			pst=DbUtil.getcon().prepareStatement(QueryUtil.updateTotalRooms);
			pst.setInt(1, info.getTotalRooms());
			pst.setInt(2, info.getHotelId());
			result=pst.executeUpdate();
			
		}
		catch(ClassNotFoundException | SQLException e) {
			//System.out.println("Exception occurs in reservation");
			e.printStackTrace();
		}
		return result;
	}
	@Override
	public int editTotalRoomsAndCost(HotelDetails info) {
		result=0;
		try {
			pst=DbUtil.getcon().prepareStatement(QueryUtil.UpdateTotalRoomsAndCost);
			pst.setInt(1, info.getTotalRooms());
			pst.setDouble(2,info.getRoomCost());
			pst.setInt(3, info.getHotelId());
			result=pst.executeUpdate();
		}
		catch(ClassNotFoundException|SQLException e) {
			//System.out.println("Exception occurs in update");
			e.printStackTrace();
		}
		return result;
	}
	
}
